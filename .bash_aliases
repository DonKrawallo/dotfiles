## .bash_aliases

## always prompt selinux labels
alias ls='ls -Z --color=auto'
alias la='ls -aZ --color=auto'
alias ll='ls -lZ --color=auto'
## copy/move file and relabel these files automatical
## uncomment if you're using a distro with
## SELinux enabled
## otherwise there is no need to
# alias cp='cp -Z'
# alias mv='mv -Z'

## navigation
alias ...='cd ../..'
alias ....='cd ../../'
alias -- --='cd -'


## some fun stuff, more or less
alias rot13='tr a-zA-Z n-za-mN-ZA-M'
alias please='sudo '
alias wtf='whoami && whereami'
alias distro-info='lsb_release -irc && echo "Kernel-Version: `uname -r`"'
alias gping='ping -c 4 www.google.com'
