## .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

for file in ~/.{path,bash_prompt,exports,aliases,bash_aliases,functions,extra}; do
  [ -f "$file" ] && [ -r "$file" ] && source "$file"
done
unset file
